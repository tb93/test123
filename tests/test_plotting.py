import os
import platform
from distutils.version import LooseVersion

import numpy as np
import pytest

import pyvista
import vtk
from qtpy.QtWidgets import QAction, QFrame, QMenuBar, QToolBar, QVBoxLayout
from qtpy.QtCore import Qt
from qtpy.QtWidgets import QTreeWidget, QStackedWidget, QCheckBox
from pyvista import rcParams
from pyvista.plotting import Renderer

import pyvistaqt
from pyvistaqt import BackgroundPlotter, MainWindow, QtInteractor

#from pyvistaqt.plotting import (Counter, QTimer, QVTKRenderWindowInteractor,
#                                _create_menu_bar, _check_type)

from pyvistaqt.editor import Editor

def test_depth_peeling(qtbot):
    plotter = BackgroundPlotter()
    qtbot.addWidget(plotter.app_window)
    assert not plotter.renderer.GetUseDepthPeeling()
    plotter.close()
    rcParams["depth_peeling"]["enabled"] = True
    plotter = BackgroundPlotter()
    qtbot.addWidget(plotter.app_window)
    assert plotter.renderer.GetUseDepthPeeling()
    plotter.close()
    rcParams["depth_peeling"]["enabled"] = False


def test_off_screen(qtbot):
    plotter = BackgroundPlotter(off_screen=False)
    qtbot.addWidget(plotter.app_window)
    assert not plotter.ren_win.GetOffScreenRendering()
    plotter.close()
    plotter = BackgroundPlotter(off_screen=True)
    qtbot.addWidget(plotter.app_window)
    assert plotter.ren_win.GetOffScreenRendering()
    plotter.close()


def test_smoothing(qtbot):
    plotter = BackgroundPlotter()
    qtbot.addWidget(plotter.app_window)
    assert not plotter.ren_win.GetPolygonSmoothing()
    assert not plotter.ren_win.GetLineSmoothing()
    assert not plotter.ren_win.GetPointSmoothing()
    plotter.close()
    plotter = BackgroundPlotter(
        polygon_smoothing=True,
        line_smoothing=True,
        point_smoothing=True,
    )
    qtbot.addWidget(plotter.app_window)
    assert plotter.ren_win.GetPolygonSmoothing()
    assert plotter.ren_win.GetLineSmoothing()
    assert plotter.ren_win.GetPointSmoothing()
    plotter.close()